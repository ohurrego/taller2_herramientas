describe('Los estudiantes login', function() {

  beforeEach(function(){
  })

context('Login and register test', function(){
  beforeEach(function(){
    cy.visit('https://losestudiantes.co')
    cy.contains('Cerrar').click()
    cy.contains('Ingresar').click()
  })

  it('Visits los estudiantes and fails as login', function() {
    cy.screenshot()
    cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail@example.com")
    cy.get('.cajaLogIn').find('input[name="password"]').click().type("1234")
    cy.get('.cajaLogIn').contains('Ingresar').click()
    cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
    cy.screenshot()
  })

  it('Correct Login', function() {
    cy.screenshot()
    cy.get('.cajaLogIn').find('input[name="correo"]').click().type("oh.urrego@uniandes.edu.co")
    cy.get('.cajaLogIn').find('input[name="password"]').click().type("Avril112")
    cy.get('.cajaLogIn').contains('Ingresar').click()
    cy.contains('Ingresar').should('not.exist')
    cy.screenshot()
  })

  it('Fail to create account whit existing user', function() {
    cy.screenshot()
    cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Oscar")
    cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Urrego")
    cy.get('.cajaSignUp').find('input[name="correo"]').click().type("oh.urrego@uniandes.edu.co")
    cy.get('.cajaSignUp').get('.no_bold').find('input[type="checkbox"]').check()
    cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
    cy.get('.cajaSignUp').find('input[name="password"]').click().type("Avril113")
    cy.get('.cajaSignUp').find('input[name="acepta"]').check()
    cy.contains('Registrarse').click()
    cy.contains("Error: Ya existe un usuario registrado con el correo 'oh.urrego@uniandes.edu.co'")
    cy.screenshot()
  })

})

// context('Search test', function(){

//   beforeEach(function(){
//     cy.visit('https://losestudiantes.co')
//     cy.contains('Cerrar').click()
//     cy.get('.buscador').find('div[class="Select-placeholder"]').click()
//   })

//   it('Teacher Search Test', function() {
//     cy.get('.buscador').find('div[class="Select-input"]').get('input').click({force:true}).type('Tristram Bogart', {force:true})
//     cy.get('.Select-menu-outer').should('be.visible')
//     cy.wait(2000)
//     cy.get('.Select-menu-outer').should('contain', "Tristram Bogart")
//   })

//   it('Teacher Page Test', function() {
//     cy.get('.buscador').find('div[class="Select-input"]').get('input').click({force:true}).type('Tristram Bogart', {force:true})
//     cy.get('.Select-menu-outer').contains('Tristram Bogart').click()
//     cy.contains('Tristram Bogart')
//   })

// })

})