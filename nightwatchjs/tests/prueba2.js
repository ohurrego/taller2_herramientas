module.exports = { // adapted from: https://git.io/vodU0
    'Los estudiantes login ok': function(browser) {
      browser
        .url('https://losestudiantes.co/')
        .click('.botonCerrar')
        .waitForElementVisible('.botonIngresar', 6000)
        .click('.botonIngresar')
        .setValue('.cajaSignUp input[name="nombre"]', 'Oscar')
        .setValue('.cajaSignUp input[name="apellido"]', 'Urrego')
        .setValue('.cajaSignUp input[name="correo"]', 'oh.urrego@uniandes.edu.co')
        .setValue('.cajaSignUp input[name="idDepartamento"]', 'Ingeniería de Sistemas')
        .setValue('.cajaSignUp input[name="password"]', 'Avril113')
        .click('.cajaSignUp input[name="acepta"]')
        .click('.cajaSignUp .logInButton')
        .waitForElementVisible('.text-muted lead', 6000)
        .assert.containsText('.text-muted lead', 'Error: Ya existe un usuario registrado con el correo \'oh.urrego@uniandes.edu.co\'')
      browser.end();
    }
  };